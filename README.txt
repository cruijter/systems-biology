This project was done in the fourth block of the second year, from the study bio-informatics. The project was to use the data from a research that used deSolve, to recreate the results. There was also a possibility to come up with new research questions for the project.

# dataBy.csv
This is a csv file that contained the number of incidences for the bubonic, pneumonic, septicemic and unknown plague. It showed the number of incidences for the plague per day from the first of August 2017, until the twenty-sixth of August 2017.

# model.jpg
This is an image, used from the research that was redone. It shows the model of the plague outbreak.

# plague.Rmd
The Rmarkdown script with all the full report to answer the research questions for this project.

# plague.pdf
A pdf file made from the Rmarkdown script.

# preamble-latex.tex
# A TeX file to place the images, both loaded in R and the own images made, on the place in the report where they were loaded or made.

# tmpByDay.csv
A file that contained the minimum and maximum temperature on a day, respectively indicated with 'lo', 'hi'.

# yersinia_pestis.jpg
An image of the bacterium yersinia pestis. This image was used on the front page of the report.